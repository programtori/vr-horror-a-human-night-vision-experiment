﻿Shader "Custom/EyeShader" {
Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		
		CGINCLUDE
			//#pragma fragmentoption ARB_precision_hint_fastest
			#pragma target 5.0
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _MainTex_TexelSize;
			
			sampler2D _LumTex;
			float4 _LumTex_ST;
			
			float _Adaptation;
			float _Key, _White, _Limit;
			
			struct v2f {
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
			};
			
			v2f vert(appdata_img v) 
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.texcoord.xy;
				return o;
			}
		ENDCG
		
		// Main pass
		Pass {
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
			
				float4 frag(v2f i) : COLOR 
				{
					half4 cColor = tex2D(_MainTex, i.uv);		
					float4 cLum = tex2D(_LumTex, i.uv);	
					float lMin = exp(cLum.x);
					float lMax = exp(cLum.y);
					float lAvg = exp(cLum.z);
					lAvg = max(lMax / 2, _Limit); // force override for dark scene
					
					float lum = max(0.00001, Luminance(cColor.rgb));
		
					float scaled = _Key / lAvg * lum;
					scaled *= (1 + scaled / _White / _White) / (1+scaled);
					return scaled * cColor;
				}
			ENDCG
		}
		
		// Downsample pass
		Pass {
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment fragDownsample
			
				float4 fragDownsample(v2f i) : COLOR 
				{
					float4 v1 = tex2D(_MainTex, i.uv + _MainTex_TexelSize.xy * float2(-1,-1));		
					float4 v2 = tex2D(_MainTex, i.uv + _MainTex_TexelSize.xy * float2(1,1));		
					float4 v3 = tex2D(_MainTex, i.uv + _MainTex_TexelSize.xy * float2(-1,1));		
					float4 v4 = tex2D(_MainTex, i.uv + _MainTex_TexelSize.xy * float2(1,-1));	
			
					float mn = min(min(v1.x,v2.x), min(v3.x,v4.x));
					float mx = max(max(v1.y,v2.y), max(v3.y,v4.y));
					//float avg = (v1.z+v2.z+v3.z+v4.z) / 4;
					
					//avg= exp((log(v1.z)+log(v2.z)+log(v3.z)+log(v4.z))/4);
					return float4(mn, mx, 1, 1);
				}
			ENDCG
		}
		
		// Update pass
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment fragUpdate
			
				float4 fragUpdate(v2f i) : COLOR 
				{
					half2 cColor = tex2D(_MainTex, i.uv).xy;		
					return half4(cColor.xyx, _Adaptation);
				}
			ENDCG
		}
		
		// Prepare pass
		Pass {
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment fragPrepare
			
				float4 fragPrepare(v2f i) : COLOR 
				{
					float v = tex2D(_MainTex, i.uv);
					float l = log(v + 0.001);
					return half4(l, l, l, 1);
				}
			ENDCG
		}
	} 
	FallBack Off
}