//@script RequireComponent(CharacterController); //Easy Footsteps will not work as intended without a character controller
@script RequireComponent(AudioSource); //How are you going to play the sounds, eh?

//I believe that it is not hard to expand this script to suit your needs even more, well that´s why it is called
//Easy Footsteps. The point is, you shouldn´t even touch the Footstep function, just the update, and the update
//is extremely simple.


//private var Controller : CharacterController;

private var FootstepHit : RaycastHit;
private var RayDown : Vector3;

private var Leg : int = 0; //The leg which is now moving, used to alternate between footstep sounds.

private var Loop1 : int = 0; //A sort of workaround :(
private var Loop2 : int = 0; //A sort of workaround to my mediocore programming knowledge :/
private var Loop3 : int = 0; //A sort of workaround, these loops just don´t want to cooperate :@
private var CheckLoop : int = 0;	//Another workaround for the startup checks.... ya don´t want ya console flooded with the mighty

var Speed : float = 2; //Insert the Character Controller´s movement speed, so the footsteps work at a correct rate.
					   //2 is a good starting value
					   //You can write a script that controls this value directly to the movement speed of the controller.
					   
var StepLength : float = 1; //How long do you want the step be, this controls the rate the footsteps are playing at.
							//1 is a good starting value
							
var LegLength : float = 1.5;	//How long should the tag detection raycast should be, 1.5 is a good value
								//As it lets on surfaces a little below also play the correct sound.
								//10 - on the other hand, is way too much - I recommend experimenting with this value.

private var CurStepLength : float = 0; //This value is controlled by the Speed. How long to next footstep?

var DefaultLeg1FootStep : AudioClip; //Used when untagged.
var DefaultLeg2FootStep : AudioClip; //Used when untagged.

var UseTerrainFootsteps : boolean = true;
var TerrainFootsteps : AudioClip[];

var UseTagDependantSounds : boolean = false;
var UseDualLegMode : boolean = false;

var Leg1Footsteps : AudioClip[];	//Footstep sounds used for the first leg.
var Leg2Footsteps : AudioClip[];	//Footstep sounds used for the second leg.
var FootstepTags : String[];		//Tags used for the corresponding footstep!

var monster_anim : Animator;

function Start () {
	//Controller = GetComponent(CharacterController); //Cache the CharacterController for quicker reference.
	RayDown = transform.TransformDirection (Vector3.down); //Cache the down direction, for quicker raycasting.
	//StartupCheck();
	monster_anim = this.GetComponent("Animator");
}

function Update () {
	//If CurStepLength is bigger than StepLength then call Footstep
	if(CurStepLength >= StepLength) {
		Footstep();
	}
	if(monster_anim.GetInteger("Tracking_State") == 0)
	{
		Speed = 3;
		StepLength = 2;
	}
	else
	{
		Speed = 2;
		StepLength = 2.4;
	}
	//If the player moves Horizontal or Vertical axis and the controller is grounded, play the footsteps!
	CurStepLength += Speed * Time.deltaTime;
}


function Footstep () {
	//PLAY THE FOOTSTEP ONLY IF THE CHARACTER CONTROLLER IS GROUNDED!
	//if(Controller.isGrounded) {
		if(UseTagDependantSounds == false) { //play the default footstep sound all the time.
			if(Leg == 0) {
				audio.PlayOneShot(DefaultLeg1FootStep);
			}
			if(Leg == 1) {
				audio.PlayOneShot(DefaultLeg2FootStep);
			}
		}
	//}
	//Switch Legs.
	CurStepLength = 0;
	if(Leg == 0) {
		Leg = 1;
	} else {
		Leg = 0;
	}
}