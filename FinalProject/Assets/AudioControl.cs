﻿using UnityEngine;
using System.Collections;

public class AudioControl : MonoBehaviour {

	public AudioSource audio;
	public AudioClip backMusic;
	// Update is called once per frame wheeee
	private bool button_press = false;
	private bool in_range = false;
	void Start()
	{
		audio.Play ();
	}
	void Update()
	{
		if (in_range) {
			if (!button_press && OVRGamepadController.GPC_GetButton (OVRGamepadController.Button.Y)) {
				if (audio.mute)
					audio.mute = false;
				else
					audio.mute = true;
				button_press = true;
			} else if (button_press && !OVRGamepadController.GPC_GetButton (OVRGamepadController.Button.Y))
				button_press = false;
		}
	}
	void OnTriggerEnter (Collider col) {
		if (col.tag == "Siren") {
			in_range = true;
		}
	}
	void OnTriggerExit(Collider col)
	{
		if(col.tag == "Siren")
		{
			in_range = false;
		}
	}
}
