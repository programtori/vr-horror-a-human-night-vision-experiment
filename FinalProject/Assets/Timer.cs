﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {
	public Light flashlight;
	// Use this for initialization
	void Start () {
		StartCoroutine ("Lights_out");
	}
	
	// Update is called once per frame
	private IEnumerator Lights_out()
	{
		while (true) {
			yield return new WaitForSeconds(3.0f);
			if (flashlight.enabled) {
				int off_chance = (int)Random.Range(0, 10);
				if(off_chance == 1)
					flashlight.enabled = false;
			}
		}
	}
}
