﻿using UnityEngine;
using System.Collections;

public class Flashlight : MonoBehaviour {

	public Light flashlight;
	public Transform targetTransform;

	bool flash_on = false;
	// Use this for initialization
	void Start () {
		//flashlight = (Light)GetComponent ("FlashLight");
		flashlight.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!flash_on && (Input.GetKeyDown (KeyCode.F) || OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.A))) {
			flash_on = true;
			if(flashlight.enabled)
			{

				flashlight.enabled = false;
			}
			else
			{
					flashlight.enabled = true;
			}
		}
		if (flash_on && (Input.GetKeyUp (KeyCode.F) || !(OVRGamepadController.GPC_GetButton (OVRGamepadController.Button.A))))
			flash_on = false;
		//Vector3 targetPosition = targetTransform.forward;
		//targetPosition.y = transform.position.y;
		//transform.LookAt(targetPosition);

	}
}
