﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Keys : MonoBehaviour {

	// Use this for initialization
	public List<Transform> key_list;
	private Transform currentKey;
	private bool key_is_correct;
	private bool key_on;
	public static int key_count;
	public Transform dir;

	public GameObject stage2;
	public GameObject stage3;
	public GameObject stage4;
	private int creepy_level = 0;

	void Start () {
		key_is_correct = false;
		currentKey = key_list [0];
		key_on = false;
		key_count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (!key_on && (Input.GetKeyDown (KeyCode.T) || OVRGamepadController.GPC_GetButton (OVRGamepadController.Button.B))) {
			key_on = true;
			if(key_is_correct)
			{
				Vector3 direction = new Vector3(currentKey.position.x - transform.position.x, currentKey.position.y - 6.12f, currentKey.position.z - transform.position.z);
				float angle = Vector3.Angle(direction, dir.forward);
				if(angle < 90.0f * 0.5f)
				{
					if(currentKey.gameObject.activeSelf)
					{
						key_count++;
						currentKey.gameObject.SetActive(false);
					}
				}

			}
		}
		if (key_on && (Input.GetKeyUp (KeyCode.R) || !(OVRGamepadController.GPC_GetButton (OVRGamepadController.Button.B))))
			key_on = false;

		if (key_count == 2 && creepy_level == 0) {
			stage2.SetActive (true);
			creepy_level+=1;
		}
		if (key_count == 4 && creepy_level == 1) {
			stage3.SetActive(true);
			creepy_level+=1;
		}
		if (key_count == 6 && creepy_level == 2) {
			stage4.SetActive(true);
			creepy_level+=1;
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Key") {
			currentKey = col.gameObject.transform;
			key_is_correct = true;
		}
	}
	void OnTriggerExit(Collider col)
	{
		if (col.tag == "Key") {
			key_is_correct = false;
		}
	}
}
