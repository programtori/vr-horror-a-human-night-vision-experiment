﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour {

	// Use this for initialization
	bool ready = false;
	void Start () {
		StartCoroutine (waiter());
	}
	
	// Update is called once per frame
	void Update () {

		if (ready && OVRGamepadController.GPC_GetButton (OVRGamepadController.Button.Start)) {
			Application.LoadLevel ("Level One");
		}
	}

	IEnumerator waiter()
	{
		yield return new WaitForSeconds (2);
		ready = true;
	}
}
