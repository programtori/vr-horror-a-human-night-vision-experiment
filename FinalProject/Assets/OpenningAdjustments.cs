﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class OpenningAdjustments : MonoBehaviour
{
	public float rightContrast;
	public float leftContrast;
	public float rightBright;
	public float leftBright;

	void Start ()
	{
		rightContrast = 0;
		leftContrast = 0;
		DontDestroyOnLoad (this);
	}
	
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Keypad1)) {
				rightContrast += 0.5f;
		}
		if (Input.GetKeyDown (KeyCode.Keypad7)) {
			rightContrast -= 0.5f;
		}
		if (Input.GetKeyDown (KeyCode.Keypad3)) {
			leftContrast += 0.5f;
		}
		if (Input.GetKeyDown (KeyCode.Keypad9)) {
			leftContrast -= 0.5f;
		}
		if (Input.GetKeyDown (KeyCode.U)) {
			rightBright += 1;
		}
		if (Input.GetKeyDown (KeyCode.J)) {
			rightBright -= 1;
		}
		if (Input.GetKeyDown (KeyCode.O)) {
			leftBright += 1;
		}
		if (Input.GetKeyDown (KeyCode.L)) {
			leftBright -= 1;
		}
		
	}
}