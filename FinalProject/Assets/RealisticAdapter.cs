﻿using UnityEngine;

[ExecuteInEditMode]
public class RealisticAdapter : MonoBehaviour
{
	public Shader shader;
	public int InitialSampling = 256;
	public float AdaptationCoefficient = 0.01f;
	public float Key = 0.5f, White = 0.5f, Limit = 0.02f;
	public int LuminanceGridSize = 1;
	private Camera mainCam;
	private Material material;
	private const int PASS_MAIN = 0;
	private const int PASS_DOWNSAMPLE = 1;
	private const int PASS_UPDATE = 2;
	private const int PASS_PREPARE = 3;
	private RenderTexture lumBuffer;
	
	void Start ()
	{
		mainCam = camera;
		mainCam.depthTextureMode |= DepthTextureMode.DepthNormals;
		material = new Material (shader);
	}
	
	[ImageEffectTransformsToLDR]
	void OnRenderImage (RenderTexture source, RenderTexture destination)
	{
		RenderTextureFormat rtFormat = RenderTextureFormat.ARGBFloat;
		if (lumBuffer == null) {
			lumBuffer = new RenderTexture (LuminanceGridSize, LuminanceGridSize, 0, rtFormat, RenderTextureReadWrite.Default);
		}
		
		RenderTexture currentTex = RenderTexture.GetTemporary (InitialSampling, InitialSampling, 0, rtFormat, RenderTextureReadWrite.Default);
		Graphics.Blit (source, currentTex, material, PASS_PREPARE);
		
		int currentSize = InitialSampling;
		while (currentSize > LuminanceGridSize) {
			RenderTexture next = RenderTexture.GetTemporary (currentSize / 2, currentSize / 2, 0, rtFormat, RenderTextureReadWrite.Default);
			Graphics.Blit (currentTex, next, material, PASS_DOWNSAMPLE);
			//Graphics.Blit (currentTex, next);
			RenderTexture.ReleaseTemporary (currentTex);
			currentTex = next;
			currentSize /= 2;
		}
		
		if (!lumBuffer.IsCreated ()) {
			Debug.Log ("Luminance recreate");
			lumBuffer.Create ();
			Graphics.Blit (currentTex, lumBuffer);
		} else {
			material.SetFloat ("_Adaptation", AdaptationCoefficient);
			if (Application.isEditor && !Application.isPlaying) 
				Graphics.Blit (currentTex, lumBuffer);
			else
				Graphics.Blit (currentTex, lumBuffer, material, PASS_UPDATE);
		}
		
		material.SetTexture ("_LumTex", lumBuffer);
		material.SetFloat ("_Key", Key);
		material.SetFloat ("_White", White);
		material.SetFloat ("_Limit", Limit);
		Graphics.Blit (source, destination, material, PASS_MAIN);
		//Graphics.Blit (lumBuffer, destination);
		
		RenderTexture.ReleaseTemporary (currentTex);
	}		
}
