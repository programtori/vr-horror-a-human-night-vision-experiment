var blankTexture : Texture2D;
var intensity : float = 0.5;
var red = 0.5;
var green = 0.0;
var blue = 0.0;

function OnGUI()
{
	
	prevColor = GUI.color;
	GUI.color = new Color(red, green, blue, intensity);
	GUI.DrawTexture(Rect(0.0f, 0.0f, Screen.width, Screen.height), blankTexture);
	
}