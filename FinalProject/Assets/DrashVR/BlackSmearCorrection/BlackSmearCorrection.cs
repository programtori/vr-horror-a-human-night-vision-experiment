﻿using UnityEngine;

public class BlackSmearCorrection : MonoBehaviour
{
	[Range(-100f, 100f)]
	public float correctionContrastLevelLeft = -2f;
	[Range(-100f, 100f)]
	public float correctionBrightnessLevelLeft = 10f;
	public float smoothTime = 0.1f;
	public bool initiallyApplyCorrection = true;
	public KeyCode keyToToggleCorrection = KeyCode.F10;

	[Range(-100f, 100f)]
	public float correctionContrastLevelRight = -2f;
	[Range(-100f, 100f)]
	public float correctionBrightnessLevelRight = 10f;

	private BlackSmearCorrectionEffect[] blackSmearCorrectionEffects = null;
	private bool isApplyingCorrection = true;
	private float targetContrast = 0f;
	private float currentContrast = 0f;
	private float currentContrastVelocity = 0f;
	private float targetBrightness = 0f;
	private float currentBrightness = 0f;
	private float currentBrightnessVelocity = 0f;

	private float targetContrast2 = 0f;
	private float currentContrast2 = 0f;
	private float currentContrastVelocity2 = 0f;
	private float targetBrightness2 = 0f;
	private float currentBrightness2 = 0f;
	private float currentBrightnessVelocity2 = 0f;
	private GameObject constants;
	private OpenningAdjustments sc;

	void Start()
	{
		blackSmearCorrectionEffects = GetComponentsInChildren<BlackSmearCorrectionEffect>(true);
		constants = GameObject.Find ("ContrastVariables");
		sc = constants.GetComponent<OpenningAdjustments>();
		isApplyingCorrection = initiallyApplyCorrection;
		correctionContrastLevelLeft = sc.leftContrast;
		correctionContrastLevelRight = sc.rightContrast;
		correctionBrightnessLevelLeft = sc.leftBright;
		correctionBrightnessLevelRight = sc.rightBright;
		SetUpCorrection();
		currentContrast  = targetContrast;
		currentContrast2  = targetContrast2;
		currentContrastVelocity = 0f;
		currentBrightness = targetBrightness;
		currentBrightnessVelocity = 0f;
	}

	void Update()
	{
		targetContrast2 = sc.leftContrast;
		targetContrast = sc.rightContrast;
		targetBrightness2 = sc.leftBright;
		targetBrightness = sc.rightBright;
		correctionContrastLevelLeft = sc.leftContrast;
		correctionContrastLevelRight = sc.rightContrast;
		correctionBrightnessLevelLeft = sc.leftBright;
		correctionBrightnessLevelRight = sc.rightBright;
		if(keyToToggleCorrection != KeyCode.None && Input.GetKeyDown(keyToToggleCorrection))
		{
			isApplyingCorrection = !isApplyingCorrection;
			SetUpCorrection();
		}

		if(currentContrast != targetContrast)
		{
			currentContrast = Mathf.SmoothDamp(currentContrast, targetContrast, ref currentContrastVelocity, smoothTime, 100f, Time.deltaTime);
			if(Approximately(currentContrast, targetContrast, 0.01f))
				currentContrast = targetContrast;
		}

		if(currentBrightness != targetBrightness)
		{
			currentBrightness = Mathf.SmoothDamp(currentBrightness, targetBrightness, ref currentBrightnessVelocity, smoothTime, 100f, Time.deltaTime);
			if(Approximately(currentBrightness, targetBrightness, 0.01f))
				currentBrightness = targetBrightness;
		}

		if(currentContrast2 != targetContrast2)
		{
			currentContrast2 = Mathf.SmoothDamp(currentContrast2, targetContrast2, ref currentContrastVelocity2, smoothTime, 100f, Time.deltaTime);
			if(Approximately(currentContrast2, targetContrast2, 0.01f))
				currentContrast2 = targetContrast2;
		}
		
		if(currentBrightness2 != targetBrightness2)
		{
			currentBrightness2 = Mathf.SmoothDamp(currentBrightness2, targetBrightness2, ref currentBrightnessVelocity2, smoothTime, 100f, Time.deltaTime);
			if(Approximately(currentBrightness2, targetBrightness2, 0.01f))
				currentBrightness2 = targetBrightness2;
		}


			blackSmearCorrectionEffects[0].contrast = currentContrast;
			blackSmearCorrectionEffects[0].brightness = currentBrightness;
			blackSmearCorrectionEffects[0].enabled = (currentContrast != 0f || currentBrightness != 0f);


		blackSmearCorrectionEffects[1].contrast = currentContrast2;
		blackSmearCorrectionEffects[1].brightness = currentBrightness2;
		blackSmearCorrectionEffects[1].enabled = (currentContrast2 != 0f || currentBrightness2 != 0f);
	}

	private void SetUpCorrection()
	{
		if(isApplyingCorrection)
		{
			targetContrast = correctionContrastLevelLeft;
			targetBrightness = correctionBrightnessLevelLeft;
			targetContrast2 = correctionContrastLevelRight;
			targetBrightness2 = correctionBrightnessLevelRight;
		}
		else
		{
			targetContrast = 0f;
			targetBrightness = 0f;
			targetContrast2 = 0f;
			targetBrightness2 = 0f;
		}
	}

	private bool Approximately(float value, float about, float range = 0.001f) 
	{
		return ((Mathf.Abs(value - about) < range));
	}
}
