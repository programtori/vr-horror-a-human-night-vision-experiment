﻿using UnityEngine;
using System.Collections;

public class EnemySight : MonoBehaviour
{
	public float fieldOfViewAngle = 360f;           // Number of degrees, centred on forward, for the enemy see.
	public bool playerInSight;                      // Whether or not the player is currently sighted.
	//private NavMeshAgent nav;                       // Reference to the NavMeshAgent component.
	//private Animator anim;                          // Reference to the Animator.
	//public static Transform lastPlayerSighting;  // Reference to last global sighting of the player.
	public GameObject player;   
	// Reference to the player.
	//private Animator playerAnim;                    // Reference to the player's animator component.
	//private PlayerHealth playerHealth;              // Reference to the player's health script.
	//private HashIDs hash;                           // Reference to the HashIDs.
	//private Vector3 previousSighting;               // Where the player was sighted last frame.
	
	
	void Awake ()
	{
	}
	
	
	void Update ()
	{
		playerInSight = false;
		Vector3 direction = player.transform.position - new Vector3(0, player.transform.position.y -2,0) - transform.position;

		Vector3 normal = transform.position - new Vector3 (0, transform.position.y - 2, 0);
		float angle = Vector3.Angle(direction, transform.forward);
		//if(angle < fieldOfViewAngle * 0.5f)
		//{
		RaycastHit hit;	
		if(Physics.Raycast(transform.position, direction, out hit))
		{
			if(hit.collider.gameObject == player)
			{
				playerInSight = true;
				//lastPlayerSighting.position = player.transform.position;
			}
		}
		//}

	}


}