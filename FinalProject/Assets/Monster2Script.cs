﻿using UnityEngine;
using System.Collections;

public class Monster2Script : MonoBehaviour {

	public AudioClip angry1;
	public AudioClip angry2;
	public AudioClip upset1;
	public AudioClip upset2;
	
	public AudioSource monst_source;
	private AIPath aiScript;
	
	private bool did_cry = false;
	private bool stop_sound = false;
	
	// Update is called once per frame
	void Start() {
		aiScript = this.GetComponent<AIPath> ();
	}
	void Update () {
		if (!stop_sound && !did_cry && (aiScript.user_target == aiScript.target)) {
			monst_source.clip = upset2;
			did_cry = true;
			monst_source.pitch = 1;
			monst_source.Play();
		} else if (did_cry && (aiScript.user_target != aiScript.target)) {
			did_cry = false;
		}
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			stop_sound = true;
		}
	}
}
