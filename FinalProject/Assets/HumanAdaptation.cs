﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class HumanAdaptation : MonoBehaviour
{
	public List<Light> light_list;
	public Light flashlight;
	public Light User_light;
	private Color lowLight;
	private Color highLight;
	private Color light1, light2;
	private float duration_light = 20;
	private float duration_angle = 2;
	private float t1 = 0;
	private float t2 = 0;

	private float intensity_light_u_1 = 0.0f;
	private float intensity_light_u_2 = 0;

	private float max_intensity = 3;
	private float low_intensity = 2;
	private float intensity1, intensity2;

	private float max_angle = 80;
	private float low_angle = 60;
	private float angle1, angle2;

	public List<GameObject> monster;
	private AIPath[] aiScript = new AIPath[4];
	private bool[] init_monsters = new bool[4];

	void Start ()
	{
		lowLight = new Color(0.01f, 0.01f, 0.01f);
		highLight = new Color (0.07f, 0.07f, 0.07f);
		//RenderSettings.ambientLight = highLight;
		light1 = highLight;
		light2 = lowLight;
		intensity1 = low_intensity;
		intensity2 = max_intensity;
		angle1 = max_angle;
		angle2 = low_angle;
		init_monsters [0] = false;
		init_monsters [1] = false;
		init_monsters [2] = false;
		init_monsters [3] = false;
		//aiScript[1] = monster[1].GetComponent<AIPath> ();
	}

	private bool flash_change = false;
	private bool acceptingChange = false;
	private bool light_change = false;
	void Update()
	{
		if (flashlight.enabled && !flash_change) {
			flash_change = true;
		}
		else if (!flashlight.enabled && flash_change) {
			flash_change = false;
		}
		if (acceptingChange && (flash_change || light_change)) { //need to turn light iff: in light or turned on flashlight and is not already light
			light2 = lowLight;
			light1 = RenderSettings.ambientLight; //go from highlight to low light
			t1 = 0;
			acceptingChange = false;
			duration_light = 2;
			intensity_light_u_1 = User_light.intensity;
			intensity_light_u_2 = 0;
		} else if(!acceptingChange && (!flash_change && !light_change)){ //need to turn off light iff: it is night and the flash_light and in light are not true
			light1 = RenderSettings.ambientLight;
			light2 = highLight; //go from lowlight to high light
			t1 = 0;
			duration_light = 10;
			acceptingChange = true;
			intensity_light_u_1 = User_light.intensity;
			intensity_light_u_2 = 0.05f;
		}
		//RenderSettings.ambientLight = Color.Lerp (light1, light2, t1);
		User_light.intensity = Mathf.Lerp (intensity_light_u_1, intensity_light_u_2, t1);
		foreach(Light l in light_list)
		{
			l.intensity = Mathf.Lerp(intensity1, intensity2, t2);
			l.spotAngle = Mathf.Lerp(angle1, angle2, t2);
		}
		if (t1 < 1) {
			t1 += Time.deltaTime/duration_light;
		}
		if (t2 < 1) {
			t2 += Time.deltaTime/duration_angle;
		}
		int i = 0;
		for(; i < (Keys.key_count/2)+1; i++)
		{
			if(!init_monsters[i])
			{
				aiScript[i] = monster[i].GetComponent<AIPath> ();
				init_monsters[i] = true;
			}
			aiScript[i].isLit = flash_change || light_change;
		}
	}
	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Light" && !light_change) //In the light
		{
			light_change = true;
			intensity1 = light_list[0].intensity;
			intensity2 = low_intensity;
			angle2 = max_angle;
			angle1 = light_list[0].spotAngle;
			t2 = 0;
		}
		//RenderSettings.ambientLight = Color(.001f, .001f, .001f);
	}
	void OnTriggerExit(Collider col)
	{
		if(col.tag == "Light" && light_change) //can see in dark now
		{
			light_change = false;
			intensity2 = max_intensity;
			intensity1 = light_list[0].intensity;
			angle1 = light_list[0].spotAngle;
			angle2 = low_angle;
			t2 = 0;
		}
		//RenderSettings.ambientLight = Color(.001f, .001f, .001f);
	}
}