﻿using UnityEngine;
using System.Collections;

public class MonsterAudio : MonoBehaviour {

	public AudioClip angry1;
	public AudioClip angry2;
	public AudioClip upset1;
	public AudioClip upset2;

	public AudioSource monst_source;
	private AIPath aiScript;

	private AudioClip source_clip;
	private bool did_cry = false;
	private bool stop_sound = false;

	// Update is called once per frame
	void Start() {
		aiScript = this.GetComponent<AIPath> ();
	}
	void Update () {
		if (!stop_sound && !did_cry && (aiScript.user_target == aiScript.target)) {
			int sound_choice = Random.Range(1, 5);
			switch(sound_choice)
			{
			case 1:
				source_clip = upset2;
				break;
			case 2:
				source_clip = upset1;
				break;
			case 3: 
				source_clip = angry2;
				break;
			case 4:
				source_clip = angry1;
				break;
			}
			did_cry = true;
			monst_source.pitch = 1;
			monst_source.PlayOneShot(source_clip, 0.5f);
		} else if (did_cry && (aiScript.user_target != aiScript.target)) {

			did_cry = false;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			stop_sound = true;
		}
	}
}
