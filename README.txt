Sample Horror Game with scotopic and photopic vision experiment for the Oculus Rift.  Requires Unity Pro 4 to modify.

Authors: Victoria Vanderbach and Jessica Spratley

Goal: Imitate the human eye adjustments between low light vision and day time vision in a video game setting on the Oculus Rift.

Background:
Ever walked into a dark room and couldn't see at first, but over time, you begin to be able to see despite the low light?

The human eye has mechanisms to help you see in any amount of light.  We wanted to simulate this temporary blinding sensation and the transitions in virtual reality to add a level of comfort to the VR experience.